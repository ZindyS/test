package com.example.testapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterForRecView: RecyclerView.Adapter<AdapterForRecView.VH>() {

    lateinit var list: MutableList<Pair<String, String>>

    fun setList123(list: MutableList<Pair<String, String>>) {
        this.list = list
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.findViewById(R.id.text)
        val button: Button = itemView.findViewById(R.id.button2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.textView.text = list[position].first
        holder.button.text = list[position].second
        holder.button.setOnClickListener {
            MainActivity3.doSMT(list[position].first)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}