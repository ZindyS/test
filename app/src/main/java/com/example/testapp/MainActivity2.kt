package com.example.testapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    fun onClick(v: View) {
        textView.text = "HELP"

        Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(object: Runnable {
            override fun run() {
                val intent = Intent(this@MainActivity2, MainActivity3::class.java)
                startActivity(intent)
            }
        }, 2000)
    }
}