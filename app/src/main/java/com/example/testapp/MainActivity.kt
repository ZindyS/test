package com.example.testapp

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class MainActivity : AppCompatActivity() {
    // val - const
    // var - изм
    // lateinit var
    var list = mutableListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        for (i in list) {
//
//        }
//
        Handler().postDelayed(object: Runnable {
            override fun run() {
                val intent = Intent(this@MainActivity, MainActivity2::class.java)
                startActivity(intent)
                finish()
            }
        }, 2000)
//
    }

//    fun asd() : Boolean {
//        return false
//    }
}