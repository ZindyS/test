package com.example.testapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main3.*

class MainActivity3 : AppCompatActivity() {

    companion object {
        lateinit var context: Context

        fun doSMT(str: String) {
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        val adapter = AdapterForRecView()

        val list = mutableListOf<Pair<String, String>>()

        list.add(Pair("Hello", "World"))
        list.add(Pair("Cat", "Meow"))
        list.add(Pair("Dog", "Gav"))

        adapter.setList123(list)

        recyclerView.adapter = adapter

        context = this
    }
}